#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0
import copy
import unittest
from collections import OrderedDict
from copy import deepcopy
from unittest import mock

import requests
from toolforge_weld.kubernetes import parse_quantity

from src.jobs import (
    delete_empty_harbor_tool_projects,
    delete_stale_toolforge_artifacts,
    ensure_image_retention_policy_is_set_for_project,
    get_tags_in_use_by_toolforge_deploy,
    group_repos_tags_by_version,
    make_image_retention_from_template,
    manage_harbor_projects_quotas,
    manage_harbor_tool_projects_image_retention_policy,
    retentions_are_the_same,
)
from src.utils import APIError, Config

example_image_retention_policy_template = {
    "algorithm": "or",
    "rules": [
        {
            "action": "retain",
            "params": {"latestPushedK": 5},
            "scope_selectors": {"repository": [{"decoration": "repoMatches", "kind": "doublestar", "pattern": "**"}]},
            "tag_selectors": [
                {"decoration": "matches", "extras": '{"untagged":false}', "kind": "doublestar", "pattern": "**"}
            ],
            "template": "latestPushedK",
        }
    ],
    "scope": {
        "level": "project",
        "ref": None,
    },
    "trigger": {"kind": "Schedule", "settings": {"cron": "0 */5 * * * *"}},
}


def get_dummy_params(**kwargs):
    params = {
        "config": Config(
            environment="test",
            do_retentions=True,
            auth_username="auth-username",
            auth_password="auth-password",
            base_harbor_api="https://test-harbor.com",
            toolforge_repo_artifact_limit=2,
            image_retention_policy_template=example_image_retention_policy_template,
            project_quota={"default": "1Gi", "overrides": {}},
        ),
    }
    params.update(kwargs)
    return params


class RetentionsAreTheSameTestCase(unittest.TestCase):
    project_1 = {"project_id": 1}
    project_2 = {"project_id": 2}

    def test_should_return_true_when_retentions_are_equal(self):
        retention_1 = make_image_retention_from_template(project=self.project_1, config=get_dummy_params()["config"])
        retention_2 = make_image_retention_from_template(project=self.project_1, config=get_dummy_params()["config"])
        self.assertTrue(retentions_are_the_same(retention_1, retention_2))

    def test_should_return_false_when_retentions_are_not_equal(self):
        retention_1 = make_image_retention_from_template(project=self.project_1, config=get_dummy_params()["config"])
        retention_2 = make_image_retention_from_template(project=self.project_2, config=get_dummy_params()["config"])
        self.assertFalse(retentions_are_the_same(retention_1, retention_2))


class EnsureImageRetentionPolicyIsSetForProjectTestCase(unittest.TestCase):
    dummy_params = get_dummy_params(
        harbor_project={"project_id": 0, "name": "tool-project-1", "metadata": {"retention_id": 1}}
    )

    @mock.patch("src.jobs.get_image_retention_policy", return_value={})
    @mock.patch("src.jobs.retentions_are_the_same", return_value=True)
    @mock.patch("src.jobs.make_harbor_request")
    def test_should_not_update_or_create_any_retention_policy(self, mocked_make_harbor_request, _1, _2):  # noqa: E501
        ensure_image_retention_policy_is_set_for_project(**self.dummy_params)
        self.assertFalse(mocked_make_harbor_request.called)

    @mock.patch("src.jobs.get_image_retention_policy", return_value={})
    @mock.patch("src.jobs.retentions_are_the_same", return_value=False)
    @mock.patch("src.jobs.make_harbor_request")
    def test_should_call_update_image_retention_policy(self, mocked_make_harbor_request, _1, _2):  # noqa: E501
        expected_method = "put"
        ensure_image_retention_policy_is_set_for_project(**self.dummy_params)
        self.assertTrue(mocked_make_harbor_request.called)

        gotten_method = mocked_make_harbor_request.call_args[-1].get("method", None)
        self.assertTrue(gotten_method, expected_method)

    @mock.patch("src.jobs.make_harbor_request")
    def test_should_call_create_image_retention_policy(self, mocked_make_harbor_request):  # noqa: E501
        dummy_params = copy.deepcopy(self.dummy_params)
        dummy_params["harbor_project"]["metadata"]["retention_id"] = 0
        expected_method = "post"
        ensure_image_retention_policy_is_set_for_project(**dummy_params)
        self.assertTrue(mocked_make_harbor_request.called)

        gotten_method = mocked_make_harbor_request.call_args[-1].get("method", None)
        self.assertTrue(gotten_method, expected_method)


class DeleteEmptyHarborToolProjectsTestCase(unittest.TestCase):
    dummy_params = get_dummy_params(
        harbor_projects=[{"name": "tool-project-1", "repo_count": 0}, {"name": "tool-project-2", "repo_count": 0}]
    )

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.delete_harbor_project")
    def test_should_call_delete_harbor_project_when_projects_have_no_repo(
        self, mocked_delete_project, mocked_get_projects
    ):
        mocked_get_projects.return_value = self.dummy_params["harbor_projects"]
        delete_empty_harbor_tool_projects(self.dummy_params["config"])
        self.assertEqual(mocked_delete_project.call_count, len(self.dummy_params["harbor_projects"]))

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.delete_harbor_project")
    def test_should_not_call_delete_harbor_project_when_projects_have_repo(
        self, mocked_delete_project, mocked_get_projects
    ):
        dummy_params = copy.deepcopy(self.dummy_params)
        for project in dummy_params["harbor_projects"]:
            project["repo_count"] = 1
        mocked_get_projects.return_value = dummy_params["harbor_projects"]
        delete_empty_harbor_tool_projects(dummy_params["config"])
        self.assertFalse(mocked_delete_project.called)

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.delete_harbor_project")
    def test_should_call_delete_harbor_project_once_when_one_project_has_no_repo(
        self, mocked_delete_project, mocked_get_projects
    ):
        dummy_params = copy.deepcopy(self.dummy_params)
        dummy_params["harbor_projects"][0]["repo_count"] = 1
        mocked_get_projects.return_value = dummy_params["harbor_projects"]
        delete_empty_harbor_tool_projects(dummy_params["config"])
        self.assertEqual(mocked_delete_project.call_count, 1)


class ManageHarborToolProjectsImageRetentionPolicyTestCase(unittest.TestCase):
    dummy_params = get_dummy_params(
        harbor_projects=[{"name": "tool-project-1", "repo_count": 1}, {"name": "tool-project-2", "repo_count": 1}]
    )

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.ensure_image_retention_policy_is_set_for_project")
    def test_should_not_call_function_when_do_retentions_is_false(self, mocked_ensure_retention, mocked_get_projects):
        dummy_params = copy.deepcopy(self.dummy_params)
        dummy_params["config"].do_retentions = False
        mocked_get_projects.return_value = dummy_params["harbor_projects"]
        manage_harbor_tool_projects_image_retention_policy(dummy_params["config"])
        self.assertFalse(mocked_ensure_retention.called)

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.ensure_image_retention_policy_is_set_for_project")
    def test_should_call_function_when_do_retentions_is_true(self, mocked_ensure_retention, mocked_get_projects):
        mocked_get_projects.return_value = self.dummy_params["harbor_projects"]
        manage_harbor_tool_projects_image_retention_policy(self.dummy_params["config"])
        self.assertEqual(mocked_ensure_retention.call_count, len(self.dummy_params["harbor_projects"]))

    @mock.patch("src.jobs.get_harbor_projects")
    @mock.patch("src.jobs.ensure_image_retention_policy_is_set_for_project")
    def test_should_not_call_function_when_projects_have_no_repo(self, mocked_ensure_retention, mocked_get_projects):
        dummy_params = copy.deepcopy(self.dummy_params)
        for project in dummy_params["harbor_projects"]:
            project["repo_count"] = 0
        mocked_get_projects.return_value = dummy_params["harbor_projects"]
        manage_harbor_tool_projects_image_retention_policy(dummy_params["config"])
        self.assertFalse(mocked_ensure_retention.called)


class DeleteStaleToolforgeImagesTestCase(unittest.TestCase):
    repo_name = "tool-repo-1"
    dummy_params = get_dummy_params(
        harbor_project_repos=[
            {
                "name": repo_name,
                "artifact_count": 5,
            },
        ],
        artifacts={
            repo_name: [
                {"tags": [{"name": "tag-0.0.4-xxxxxx"}]},
                {"tags": [{"name": "tag-0.0.3-xxxxxx"}]},
                {"tags": [{"name": "0.1.0"}]},
                {"tags": [{"name": "0.2.0"}]},
                {"tags": [{"name": "tag-0.0.5-xxxxxx"}]},
            ],
        },
        immutable_rule={"id": 1},
    )

    @mock.patch("src.jobs.get_repos")
    @mock.patch("src.jobs.get_repos_artifacts")
    @mock.patch("src.jobs.get_immutable_rules")
    @mock.patch("src.jobs.disable_immutable_rule")
    @mock.patch("src.jobs.enable_immutable_rule")
    @mock.patch("src.jobs.delete_harbor_artifact")
    @mock.patch("src.jobs.get_tags_in_use_by_toolforge_deploy", return_value={})
    def test_only_stale_artifacts_are_deleted(
        self,
        _1,
        mocked_delete_artifact,
        mocked_enable_immutable_rule,
        mocked_disable_immutable_rule,
        mocked_get_immutable_rules,
        mocked_get_repos_artifacts,
        mocked_get_repos,
    ):
        mocked_get_repos.return_value = self.dummy_params["harbor_project_repos"]
        mocked_get_repos_artifacts.return_value = self.dummy_params["artifacts"]
        mocked_get_immutable_rules.return_value = [self.dummy_params["immutable_rule"]]
        delete_stale_toolforge_artifacts(self.dummy_params["config"])
        self.assertEqual(
            mocked_disable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        self.assertEqual(
            mocked_enable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        # check the arguments passed for each delete_harbor_artifact call
        self.assertEqual(mocked_delete_artifact.call_args_list[0][1]["repo"], self.repo_name)
        self.assertEqual(mocked_delete_artifact.call_args_list[0][1]["tag"], "tag-0.0.3-xxxxxx")
        self.assertEqual(mocked_delete_artifact.call_args_list[1][1]["repo"], self.repo_name)
        self.assertEqual(mocked_delete_artifact.call_args_list[1][1]["tag"], "0.2.0")
        self.assertEqual(mocked_delete_artifact.call_args_list[2][1]["repo"], self.repo_name)
        self.assertEqual(mocked_delete_artifact.call_args_list[2][1]["tag"], "0.1.0")

    @mock.patch("src.jobs.get_repos")
    @mock.patch("src.jobs.get_repos_artifacts")
    @mock.patch("src.jobs.get_immutable_rules")
    @mock.patch("src.jobs.disable_immutable_rule")
    @mock.patch("src.jobs.enable_immutable_rule")
    @mock.patch("src.jobs.delete_harbor_artifact")
    @mock.patch("src.jobs.get_tags_in_use_by_toolforge_deploy", return_value={repo_name: ["tag-0.0.3-xxxxxx"]})
    def test_do_not_delete_deployed_tags(
        self,
        _1,
        mocked_delete_artifact,
        mocked_enable_immutable_rule,
        mocked_disable_immutable_rule,
        mocked_get_immutable_rules,
        mocked_get_repos_artifacts,
        mocked_get_repos,
    ):
        mocked_get_repos.return_value = self.dummy_params["harbor_project_repos"]
        mocked_get_repos_artifacts.return_value = self.dummy_params["artifacts"]
        mocked_get_immutable_rules.return_value = [self.dummy_params["immutable_rule"]]
        delete_stale_toolforge_artifacts(self.dummy_params["config"])
        self.assertEqual(
            mocked_disable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        self.assertEqual(
            mocked_enable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        # check the arguments passed for each delete_harbor_artifact call
        self.assertEqual(mocked_delete_artifact.call_args_list[0][1]["repo"], self.repo_name)
        self.assertEqual(mocked_delete_artifact.call_args_list[0][1]["tag"], "0.2.0")
        self.assertEqual(mocked_delete_artifact.call_args_list[1][1]["repo"], self.repo_name)
        self.assertEqual(mocked_delete_artifact.call_args_list[1][1]["tag"], "0.1.0")

    @mock.patch("src.jobs.get_repos")
    @mock.patch("src.jobs.get_repos_artifacts")
    @mock.patch("src.jobs.get_immutable_rules")
    @mock.patch("src.jobs.disable_immutable_rule")
    @mock.patch("src.jobs.enable_immutable_rule")
    @mock.patch("src.jobs.delete_harbor_artifact")
    @mock.patch("src.jobs.get_tags_in_use_by_toolforge_deploy", return_value={})
    def test_no_artifact_deleted_if_no_stale_artifacts(
        self,
        _1,
        mocked_delete_artifact,
        mocked_enable_immutable_rule,
        mocked_disable_immutable_rule,
        mocked_get_immutable_rules,
        mocked_get_repos_artifacts,
        mocked_get_repos,
    ):
        mocked_get_repos.return_value = self.dummy_params["harbor_project_repos"]
        artifacts = copy.deepcopy(self.dummy_params["artifacts"])
        # make repo artifacts count equal to toolforge_repo_artifact_limit
        artifacts[self.repo_name] = artifacts[self.repo_name][0:2]
        mocked_get_repos_artifacts.return_value = artifacts
        mocked_get_immutable_rules.return_value = [self.dummy_params["immutable_rule"]]
        delete_stale_toolforge_artifacts(self.dummy_params["config"])
        self.assertEqual(
            mocked_disable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        self.assertEqual(
            mocked_enable_immutable_rule.call_args_list[0][1]["rule"],
            self.dummy_params["immutable_rule"],
        )
        self.assertFalse(mocked_delete_artifact.called)


class GroupReposTagsByVersion(unittest.TestCase):
    def test_sorts_well_on_digit_number_change_versions(self):
        artifacts = {
            "repo1": [
                {"tags": [{"name": "1.99.1-artifact1-somehash"}]},
                {"tags": [{"name": "1.101.1-artifact2-somehash"}]},
                {"tags": [{"name": "1.98.1-artifact3-somehash"}]},
                {"tags": [{"name": "1.111.1-artifact4-somehash"}]},
            ]
        }
        project = "dummyproject"
        gotten_artifacts = group_repos_tags_by_version(**get_dummy_params(artifacts=artifacts, project=project))

        expected_dict = OrderedDict()
        for key, val in [
            ("1.111.1", ["1.111.1-artifact4-somehash"]),
            ("1.101.1", ["1.101.1-artifact2-somehash"]),
            ("1.99.1", ["1.99.1-artifact1-somehash"]),
            ("1.98.1", ["1.98.1-artifact3-somehash"]),
        ]:
            expected_dict[key] = val

        expected_artifacts = {"repo1": expected_dict}

        assert gotten_artifacts == expected_artifacts


class GetToolforgeDeployTagsTestCase(unittest.TestCase):
    dummy_params = get_dummy_params(component_names=["dummy-component"])

    def setUp(self):
        chart = """
            chartVersion: 0.0.60-20240902124757-342c4931
            chartRepository: tools
            certificates:
              internalClusterDomain: tools.local
            """
        self.response_json = {
            "data": {
                "project": {
                    "repository": {
                        "blobs": {"nodes": [{"path": "components/dummy-component/values/tools.yaml", "rawBlob": chart}]}
                    }
                }
            }
        }

    @mock.patch("src.jobs.requests.post")
    def test_raise_on_gitlab_api_error(self, mock_post):
        mock_response = mock.Mock()
        mock_response.status_code = 400
        mock_post.return_value = mock_response
        mock_post.return_value.raise_for_status.side_effect = requests.exceptions.HTTPError("GitLab API error")

        with self.assertRaises(APIError):
            get_tags_in_use_by_toolforge_deploy(**self.dummy_params)

    @mock.patch("src.jobs.requests.post")
    def test_raise_on_gitlab_query_error(self, mock_post):
        mock_response = mock.Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = {"errors": [{"message": "Gitlab Query Error"}]}
        mock_post.return_value = mock_response

        with self.assertRaises(APIError):
            get_tags_in_use_by_toolforge_deploy(**self.dummy_params)

    @mock.patch("src.jobs.requests.post")
    def test_all_chart_versions_in_toolforge_deploy_repo(self, mock_post):
        mock_response = mock.Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = self.response_json
        mock_post.return_value = mock_response
        expected_tags = {"dummy-component": ["0.0.60-20240902124757-342c4931", "0.0.60-20240902124757-342c4931"]}

        gotten_tags = get_tags_in_use_by_toolforge_deploy(**self.dummy_params)
        assert gotten_tags == expected_tags


class ManageHarborProjectsQuotasTestCase(unittest.TestCase):
    def setUp(self):
        self.dummy_params = get_dummy_params()

    @mock.patch("src.jobs.update_harbor_project_quota")
    @mock.patch("src.jobs.get_harbor_projects_quotas")
    def test_update_quota_different_from_default(self, mock_get_quotas, mock_update_quota):
        default_quota = int(parse_quantity(self.dummy_params["config"].project_quota.default))
        quotas = [
            {"id": 1, "hard": {"storage": int(parse_quantity("2Gi"))}, "ref": {"id": 1, "name": "tool-1"}},
            {"id": 2, "hard": {"storage": int(parse_quantity("0.5Gi"))}, "ref": {"id": 2, "name": "tool-2"}},
            {"id": 3, "hard": {"storage": int(parse_quantity(1))}, "ref": {"id": 3, "name": "tool-3"}},
            {"id": 4, "hard": {"storage": int(parse_quantity(-1))}, "ref": {"id": 4, "name": "tool-4"}},
        ]
        mock_get_quotas.return_value = deepcopy(quotas)

        expected_updated_quotas = quotas
        for quota in expected_updated_quotas:
            quota["hard"]["storage"] = default_quota

        manage_harbor_projects_quotas(self.dummy_params["config"])

        self.assertEqual(mock_update_quota.call_count, len(quotas))

        for quota in expected_updated_quotas:
            mock_update_quota.assert_any_call(config=self.dummy_params["config"], quota=quota)

    @mock.patch("src.jobs.update_harbor_project_quota")
    @mock.patch("src.jobs.get_harbor_projects_quotas")
    def test_dont_update_quota_same_as_default(self, mock_get_quotas, mock_update_quota):
        quotas = [
            {
                "id": 1,
                "hard": {"storage": int(parse_quantity(self.dummy_params["config"].project_quota.default))},
                "ref": {"id": 1, "name": "tool-1"},
            },
        ]
        mock_get_quotas.return_value = deepcopy(quotas)

        manage_harbor_projects_quotas(self.dummy_params["config"])

        self.assertEqual(mock_update_quota.call_count, 0)

    @mock.patch("src.jobs.update_harbor_project_quota")
    @mock.patch("src.jobs.get_harbor_projects_quotas")
    def test_update_quota_different_from_override(self, mock_get_quotas, mock_update_quota):
        default_quota = int(parse_quantity(self.dummy_params["config"].project_quota.default))
        overrides = {"tool-1": 1, "tool-3": -1, "tool-5": "2Gi"}
        quotas = [
            {"id": 1, "hard": {"storage": default_quota}, "ref": {"id": 1, "name": "tool-1"}},
            {"id": 2, "hard": {"storage": default_quota}, "ref": {"id": 2, "name": "tool-2"}},
            {"id": 3, "hard": {"storage": default_quota}, "ref": {"id": 3, "name": "tool-3"}},
            {"id": 4, "hard": {"storage": default_quota}, "ref": {"id": 4, "name": "tool-4"}},
            {"id": 5, "hard": {"storage": default_quota}, "ref": {"id": 5, "name": "tool-5"}},
        ]
        self.dummy_params["config"].project_quota.overrides = overrides
        mock_get_quotas.return_value = deepcopy(quotas)

        expected_updated_quotas = []
        for quota in quotas:
            if quota["ref"]["name"] in overrides:
                quota["hard"]["storage"] = int(parse_quantity(overrides[quota["ref"]["name"]]))
                expected_updated_quotas.append(quota)

        manage_harbor_projects_quotas(self.dummy_params["config"])

        self.assertEqual(mock_update_quota.call_count, len(expected_updated_quotas))

        for quota in expected_updated_quotas:
            mock_update_quota.assert_any_call(
                config=self.dummy_params["config"],
                quota=quota,
            )

    @mock.patch("src.jobs.update_harbor_project_quota")
    @mock.patch("src.jobs.get_harbor_projects_quotas")
    def test_dont_update_quota_same_as_override(self, mock_get_quotas, mock_update_quota):
        overrides = {
            "tool-1": 1,
            "tool-2": 1000,
            "tool-3": -1,
            "tool-4": "2Gi",
            "tool-5": "3Gi",
        }
        quotas = [
            {
                "id": 1,
                "hard": {"storage": int(parse_quantity(overrides["tool-1"]))},
                "ref": {"id": 1, "name": "tool-1"},
            },
            {
                "id": 2,
                "hard": {"storage": int(parse_quantity(overrides["tool-2"]))},
                "ref": {"id": 2, "name": "tool-2"},
            },
            {
                "id": 3,
                "hard": {"storage": int(parse_quantity(overrides["tool-3"]))},
                "ref": {"id": 3, "name": "tool-3"},
            },
            {
                "id": 4,
                "hard": {"storage": int(parse_quantity(overrides["tool-4"]))},
                "ref": {"id": 4, "name": "tool-4"},
            },
            {
                "id": 5,
                "hard": {"storage": int(parse_quantity(overrides["tool-5"]))},
                "ref": {"id": 5, "name": "tool-5"},
            },
        ]
        self.dummy_params["config"].project_quota.overrides = overrides
        mock_get_quotas.return_value = deepcopy(quotas)

        manage_harbor_projects_quotas(self.dummy_params["config"])

        self.assertEqual(mock_update_quota.call_count, 0)
