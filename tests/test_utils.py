#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0
import json
import unittest
from unittest import mock

from requests.exceptions import HTTPError, RequestException

from src.utils import USER_AGENT, Config, make_harbor_request


class MockResponse:
    def __init__(self, text_data: str, success: bool, status_code: int = 200):
        self.text_data = text_data
        self.success = success
        self.status_code = status_code

    def json(self):
        return json.loads(self.text_data)

    def raise_for_status(self):
        if not self.success:
            raise HTTPError(self.text_data, response=self)

    @property
    def text(self):
        return self.text_data

    @property
    def ok(self):
        return self.success


def get_dummy_params(**kwargs):
    params = {
        "config": Config(
            environment="test",
            do_retentions=True,
            auth_username="auth-username",
            auth_password="auth-password",
            base_harbor_api="https://test-harbor.com",
            toolforge_repo_artifact_limit=2,
            image_retention_policy_template={
                "example_image_retention_policy_key": "example_image_retention_policy_value"
            },
            project_quota={"default": "1Gi", "overrides": {}},
        ),
    }
    params.update(kwargs)
    return params


class MakeHarborRequestTestCase(unittest.TestCase):
    server_error_reply = MockResponse(
        text_data=('{"errors": [{"code": "ERROR-CODE", "message": "error message"}]}'), success=False, status_code=500
    )
    server_ok_reply = MockResponse(
        text_data=('[{"name":"tool-project-1"},' '{"name":"tool-project-2"},' '{"name":"tool-project-3"}]'),
        success=True,
        status_code=200,
    )

    kwargs = [
        {
            "config": get_dummy_params()["config"],
            "method": "get",
            "path": "projects",
            "error_desc": "get projects",
        },
        {
            "config": get_dummy_params()["config"],
            "method": "post",
            "path": "projects",
            "data": {"name": "test-project"},
            "error_desc": "create project",
        },
    ]

    @mock.patch("src.utils.requests.get", return_value=server_ok_reply)
    @mock.patch("src.utils.requests.post", return_value=server_ok_reply)
    def test_request_method_should_be_called_with_correct_params(self, mocked_requests_post, mocked_requests_get):
        for kwarg in self.kwargs:
            with self.subTest(kwarg["method"]):
                expected_url = f"{kwarg['config'].base_harbor_api}/{kwarg['path']}"
                expected_data = kwarg.get("data", None)
                expected_auth = (kwarg["config"].auth_username, kwarg["config"].auth_password)
                expected_user_agent = USER_AGENT

                make_harbor_request(**kwarg)
                if kwarg["method"] == "get":
                    requests_method = mocked_requests_get
                elif kwarg["method"] == "post":
                    requests_method = mocked_requests_post

                # the kwargs property for mock.call is only python>=3.8,
                # ci uses 3.7
                gotten_params = requests_method.call_args[-1]
                self.assertTrue(requests_method.called)
                self.assertEqual(gotten_params.get("url", ""), expected_url)
                self.assertEqual(gotten_params.get("auth", None), expected_auth)
                self.assertEqual(gotten_params.get("headers", {}).get("User-Agent", None), expected_user_agent)
                self.assertEqual(gotten_params.get("json", None), expected_data)

    @mock.patch("src.utils.requests.get", side_effect=RequestException)
    @mock.patch("src.utils.logger.log")
    def test_should_log_error_and_raise_exception_when_request_method_raises_exception(  # noqa: E501
        self, mocked_logger_log, mocked_requests_get
    ):
        self.assertRaises(Exception, make_harbor_request, **self.kwargs[0])
        self.assertTrue(mocked_requests_get.called)
        self.assertTrue(mocked_logger_log.called)

    @mock.patch("src.utils.requests.get", return_value=server_error_reply)
    @mock.patch("src.utils.logger.log")
    def test_should_log_error_and_raise_exception_when_server_returns_error(
        self, mocked_logger_log, mocked_requests_get
    ):
        self.assertRaises(Exception, make_harbor_request, **self.kwargs[0])
        self.assertTrue(mocked_requests_get.called)
        self.assertTrue(mocked_logger_log.called)

    @mock.patch("src.utils.requests.get", return_value=server_ok_reply)
    def test_returns_json_response_when_server_returns_ok_reply(self, mocked_requests_get):
        expected_result = self.server_ok_reply.json()

        result = make_harbor_request(**self.kwargs[0])

        self.assertTrue(mocked_requests_get.called)
        self.assertEqual(result, expected_result)

    @mock.patch("src.utils.requests.get", return_value=server_ok_reply)
    def test_returns_text_response_when_server_returns_ok_reply_but_no_json_requested(self, mocked_requests_get):
        expected_result = self.server_ok_reply.text

        result = make_harbor_request(json_response=False, **self.kwargs[0])

        self.assertTrue(mocked_requests_get.called)
        self.assertEqual(result, expected_result)
