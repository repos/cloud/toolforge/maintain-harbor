#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0

import json
import logging
from functools import wraps
from socket import gethostname
from typing import Any, Dict, Literal, Optional

import requests

from .config import Config

logger = logging.getLogger("maintain-harbor")

USER_AGENT = "WMCS Maintain-Harbor/1.0 tool-maintain-harbor@" f"{gethostname()} {requests.utils.default_user_agent()}"


class APIError(Exception):
    """Simple custom error class for api errors"""

    def __init__(self, description: str, reason: str):
        self.description = description
        self.reason = reason

    def to_str(self):
        try:
            reason = ""
            for err in json.loads(self.reason)["errors"]:
                reason += f"\n{err.get('code', '')}: {err.get('message', 'UNKNOWN')}"
            reason = reason.strip("\n")
        except json.decoder.JSONDecodeError:
            reason = self.reason
        return f"{self.description}. Reason: {reason}"


def with_api_logs(loglevel):
    """
    Handles logging for api endpoints
    """

    def decorator(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except APIError as error:
                logger.log(loglevel, "%s - %s", kwargs.get("config").environment, error.to_str())
                raise

        return inner

    return decorator


def get_headers(extra_headers: Optional[Dict[str, Any]] = None) -> requests.structures.CaseInsensitiveDict[str]:
    """
    update default headers with recommended user-agent
    """
    # https://meta.wikimedia.org/wiki/User-Agent_policy
    headers = requests.utils.default_headers()
    headers.update({"User-Agent": USER_AGENT})
    if extra_headers:
        headers.update(extra_headers)
    return headers


@with_api_logs(loglevel=logging.ERROR)
def make_harbor_request(
    config: Config,
    method: Literal["get", "post", "delete", "put"],
    path: str,
    error_desc: str,
    data: Optional[Dict[str, Any]] = None,
    json_response: bool = True,
) -> requests.Response:
    kwargs = {
        "url": f"{config.base_harbor_api}/{path}",
        "auth": (config.auth_username, config.auth_password),
        "headers": get_headers(),
        "timeout": 60,
    }

    if method in ["post", "put"]:
        kwargs["json"] = data

    method_fn = getattr(requests, method)

    try:
        response = method_fn(**kwargs)
        response.raise_for_status()
        if json_response:
            return response.json()
        else:
            return response.text

    except requests.exceptions.RequestException as error:
        reason = getattr(getattr(error, "response", object()), "text", None)
        reason = reason if reason else str(error)
        raise APIError(description=error_desc, reason=reason) from error
