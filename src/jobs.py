#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0

import copy
import json
import logging
import re
from collections import OrderedDict
from enum import Enum
from pathlib import Path
from typing import Any, Dict, List, Optional, cast

import requests
from toolforge_weld.kubernetes import parse_quantity

from .config import Config
from .utils import APIError, get_headers, logger, make_harbor_request, with_api_logs

SEMVER_REGEX = r"\b\d+\.\d+\.\d+\b"
TOOLFORGE_PROJECT_PREFIX = "tool-"


def get_harbor_projects(config: Config) -> List[Dict[str, Any]]:
    """Return list of harbor projects"""

    harbor_projects = make_harbor_request(
        config=config,
        method="get",
        path="projects?page_size=-1",
        error_desc="Request to get list of harbor projects failed",
    )

    logger.debug(f"{config.environment} - Got {len(harbor_projects)} projects!")
    return harbor_projects


def delete_harbor_project(config: Config, harbor_project: str):
    make_harbor_request(
        config=config,
        method="delete",
        path=f"projects/{harbor_project}",
        error_desc=(f"Request to delete harbor project {harbor_project} failed"),
        json_response=False,
    )

    logger.info(f"{config.environment} - Successfully deleted harbor project {harbor_project}")


def get_image_retention_policy(config: Config, retention_id: int, project_name: Optional[str] = None) -> Dict[str, Any]:
    return make_harbor_request(
        config=config,
        method="get",
        path=f"retentions/{retention_id}",
        error_desc=(f"Request to get retention policy with ID {retention_id} " f"for project {project_name} failed"),
    )


def update_image_retention_policy(config: Config, retention: Dict[str, Any], project_name: Optional[str] = None):
    make_harbor_request(
        config=config,
        method="put",
        path=f"retentions/{retention['id']}",
        data=retention,
        json_response=False,
        error_desc=(
            f"Request to update retention policy with ID {retention['id']} " f"for project {project_name} failed"
        ),
    )

    logger.debug(
        f"{config.environment} - Updated retention policy for project {project_name} as "
        f"the retention with ID {retention['id']} did not match"
    )


def create_image_retention_policy(config: Config, retention: Dict[str, Any], project_name: Optional[str] = None):
    make_harbor_request(
        config=config,
        method="post",
        path="retentions",
        data=retention,
        json_response=False,
        error_desc=("Request to create image retention policy for project " f"{project_name} failed"),
    )
    logger.debug(f"{config.environment} - Created retention policy for project {project_name}")


def get_repos(config: Config, project: str) -> List[Dict[str, Any]]:
    repos = make_harbor_request(
        config=config,
        method="get",
        path=f"projects/{project}/repositories?page_size=-1",
        error_desc=f"Request to get list of {project} project repositories failed",
    )
    # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
    logger.info(f"{config.environment} - Got {len(repos)} repositories for project {project}")
    for repo in repos:
        repo["name"] = repo["name"].split(f"{project}/")[1]  # remove project name from repo name
    return repos


def get_repo_artifacts(config: Config, project: str, repo: str) -> List[Dict[str, Any]]:
    return make_harbor_request(
        config=config,
        method="get",
        path=f"projects/{project}/repositories/{repo}/artifacts?page_size=-1",
        error_desc=f"Request to get list of {project} project repository artifacts failed",
    )


def get_immutable_rules(config: Config, project: str) -> List[Dict[str, Any]]:
    return make_harbor_request(
        config=config,
        method="get",
        path=f"projects/{project}/immutabletagrules?page_size=-1",
        error_desc=f"Request to get list of immutable rules for project {project} failed",
    )


def disable_immutable_rule(config: Config, project: str, rule: Dict[str, Any]):
    rule["disabled"] = True
    make_harbor_request(
        config=config,
        method="put",
        path=f"projects/{project}/immutabletagrules/{rule['id']}",
        data=rule,
        error_desc=f"Request to disable immutable rule {rule['id']} for project {project} failed",
        json_response=False,
    )
    # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
    logger.info(f"{config.environment} - Disabled immutable rule {rule['id']} for project {project}")


def enable_immutable_rule(config: Config, project: str, rule: Dict[str, Any]):
    rule["disabled"] = False
    make_harbor_request(
        config=config,
        method="put",
        path=f"projects/{project}/immutabletagrules/{rule['id']}",
        data=rule,
        error_desc=f"Request to enable immutable rule {rule['id']} for project {project} failed",
        json_response=False,
    )
    # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
    logger.info(f"{config.environment} - Enabled immutable rule {rule['id']} project {project}")


def delete_harbor_artifact(config: Config, project: str, repo: str, tag: str):
    make_harbor_request(
        config=config,
        method="delete",
        path=f"projects/{project}/repositories/{repo}/artifacts/{tag}",
        error_desc=(f"Request to delete artifact {tag} on {project}/{repo} failed"),
        json_response=False,
    )

    logger.debug(
        f"{config.environment} - Successfully deleted artifact {tag} on {project}/{repo}",
    )


def get_harbor_projects_quotas(config: Config) -> list[dict[str, Any]]:
    return make_harbor_request(
        config=config,
        method="get",
        path="quotas?page_size=-1",
        error_desc="Request to get all harbor projects quotas failed",
    )


def update_harbor_project_quota(config: Config, quota: dict[str, Any]):
    make_harbor_request(
        config=config,
        method="put",
        path=f"quotas/{quota['id']}",
        data=quota,
        json_response=False,
        error_desc=(f"Request to update quota with ID {quota['id']} for project {quota['ref']['name']} failed"),
    )


@with_api_logs(loglevel=logging.ERROR)
def get_tags_in_use_by_toolforge_deploy(config: Config, component_names: List[str]) -> Dict[str, List[str]]:
    gitlab_graphql_url = "https://gitlab.wikimedia.org/api/graphql"
    yaml_config_paths: List[str] = []
    yaml_gotmpl_config_paths: List[str] = []
    files: List[Dict[str, str]] = []
    headers = get_headers(extra_headers={"Content-Type": "application/json"})
    tags: Dict[str, List[str]] = {}

    for name in component_names:
        yaml_config_paths.extend(
            [
                f"components/{name}/values/local.yaml",
                f"components/{name}/values/toolsbeta.yaml",
                f"components/{name}/values/tools.yaml",
            ]
        )
        yaml_gotmpl_config_paths.extend(
            [
                f"components/{name}/values/local.yaml.gotmpl",
                f"components/{name}/values/toolsbeta.yaml.gotmpl",
                f"components/{name}/values/tools.yaml.gotmpl",
            ]
        )

    # querying for .yaml and .yaml.gotmpl files seperately to avoid gitlab query complexity error
    for config_paths in (yaml_config_paths, yaml_gotmpl_config_paths):
        query = f"""
        {{
        project(fullPath: "repos/cloud/toolforge/toolforge-deploy") {{
            repository {{
            blobs(paths: {json.dumps(config_paths)}) {{
                nodes {{
                path
                rawBlob
                }}
            }}
            }}
        }}
        }}
        """

        try:
            response = requests.post(gitlab_graphql_url, json={"query": query}, headers=headers)
            response.raise_for_status()
            if "errors" in response.json():
                raise requests.exceptions.RequestException(response.json()["errors"])

            files.extend(response.json()["data"]["project"]["repository"]["blobs"]["nodes"])

        except requests.exceptions.RequestException as error:
            reason = getattr(getattr(error, "response", object()), "text", None)
            reason = reason if reason else str(error)
            message = f'Attempt to get information about components "{component_names}" failed'
            raise APIError(description=message, reason=reason) from error

    for file in files:
        text = file["rawBlob"]
        path = Path(file["path"])
        if "chartVersion:" in text:
            tags[path.parent.parent.name] = tags.get(path.parent.parent.name, []) + [
                text.split("chartVersion:")[1].split("\n")[0].strip()
            ]

    return tags


def is_tool_project(name: str) -> bool:
    return name.startswith(TOOLFORGE_PROJECT_PREFIX)


def make_image_retention_from_template(project: Dict[str, Any], config: Config) -> Dict[str, Any]:
    image_retention = copy.deepcopy(config.image_retention_policy_template)
    cast(Dict[str, Any], image_retention["scope"])["ref"] = project["project_id"]
    return image_retention


def retentions_are_the_same(retention1: Dict[str, Any], retention2: Dict[str, Any]) -> bool:
    left_side = copy.deepcopy(retention1)
    right_side = copy.deepcopy(retention2)
    # remove the ids and next_scheduled_time, that should be the only difference
    left_side.pop("id", None)
    right_side.pop("id", None)
    left_side["trigger"]["settings"].pop("next_scheduled_time", None)
    right_side["trigger"]["settings"].pop("next_scheduled_time", None)
    return left_side == right_side


def ensure_image_retention_policy_is_set_for_project(
    harbor_project: Dict[str, Any],
    config: Config,
):
    current_retention_id = int(harbor_project.get("metadata", {}).get("retention_id", 0))
    new_retention = make_image_retention_from_template(project=harbor_project, config=config)

    try:
        if current_retention_id:
            existing_retention = get_image_retention_policy(
                retention_id=current_retention_id, config=config, project_name=harbor_project["name"]
            )
            if retentions_are_the_same(existing_retention, new_retention):
                logger.debug(
                    f"{config.environment} - Skipping project {harbor_project['name']} as it "
                    f"already has a retention (id={current_retention_id})",
                )
                return

            new_retention["id"] = current_retention_id
            update_image_retention_policy(config=config, retention=new_retention, project_name=harbor_project["name"])
        else:
            create_image_retention_policy(config=config, retention=new_retention, project_name=harbor_project["name"])

    except APIError:
        # Do not interrupt execution when api errors are encountered
        pass


def filter_repos_with_stale_artifacts(
    config: Config,
    repos: List[Dict[str, Any]],
) -> List[str]:
    repos_with_stale_artifacts = []
    for repo in repos:
        # a pre-check to avoid making requests for repos that have little to no artifacts.
        # this is not abolutely neccessary but it will save time and api calls.
        if repo["artifact_count"] > config.toolforge_repo_artifact_limit / 2:
            repos_with_stale_artifacts.append(repo["name"])
    # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
    logger.info(
        f"{config.environment} - Found {len(repos_with_stale_artifacts)} toolforge "
        "project repositories with stale artifacts",
    )
    return repos_with_stale_artifacts


def get_repos_artifacts(
    config: Config,
    repos: List[str],
    project: str,
) -> Dict[str, Any]:
    repos_artifacts = {}
    for repo in repos:
        artifacts = get_repo_artifacts(config=config, project=project, repo=repo)
        repos_artifacts[repo] = artifacts
    return repos_artifacts


def group_repos_tags_by_version(
    config: Config,
    artifacts: Dict[str, Any],
    project: str,
) -> Dict[str, Any]:
    tag_grouped_repos = {}
    for repo_name in artifacts:
        tags = []
        legacy_tags = []  # for tags that don't have the current tag name format
        sorted_tags = []
        tag_groups: OrderedDict[str, Any] = OrderedDict()
        repo_artifacts = artifacts[repo_name]
        for artifact in repo_artifacts:
            if not artifact["tags"] or len(artifact["tags"]) == 0:
                logger.warning(
                    f"{config.environment} - Artifact {artifact['digest']} on "
                    f"{project}/{repo_name} has no tags, skipping artifact...",
                )
                continue

            if len(artifact["tags"]) > 1:
                tag_names = ", ".join([tag["name"] for tag in artifact["tags"]])
                logger.warning(
                    f"{config.environment} - Artifact {artifact['digest']} on {project}/{repo_name} "
                    f"has more than one tag ({tag_names}), skipping artifact...",
                )
                continue

            tag = artifact["tags"][0]
            semver = re.search(SEMVER_REGEX, tag["name"])
            if not semver:
                logger.warning(
                    f"{config.environment} - Could not extract a valid semantic version "
                    f"from tag {tag['name']} on {project}/{repo_name}, skipping tag...",
                )
                continue

            tag["semver"] = semver.group()
            if tag["name"].endswith(tag["semver"]):
                legacy_tags.append(tag)
            else:
                tags.append(tag)

        # sorting tags by semver so semver grouping will be sorted by default
        sorted_tags.extend(
            sorted(
                tags,
                reverse=True,
                key=lambda tag: [int(vernum) for vernum in tag["semver"].split(".")],
            )
        )
        # we need to sort tags and legacy_tags separately because
        # sorting them together will lead to incorrect sort result
        sorted_tags.extend(
            sorted(
                legacy_tags,
                reverse=True,
                key=lambda tag: [int(vernum) for vernum in tag["semver"].split(".")],
            )
        )

        for tag in sorted_tags:
            tag_name = tag["name"]
            version = tag["semver"]
            # We can have more than one artifact per version
            # and each of the artifacts have different tags.
            # To handle this we group the artifacts by the semantic version.
            if tag_groups.get(version, None):
                tag_groups[version].append(tag_name)
            else:
                tag_groups[version] = [tag_name]
        tag_grouped_repos[repo_name] = tag_groups
    return tag_grouped_repos


def delete_stale_artifacts(
    config: Config,
    tag_grouped_repos: Dict[str, Any],
    project: str,
):
    rules = get_immutable_rules(config=config, project=project)
    tags_in_use_by_toolforge_deploy = get_tags_in_use_by_toolforge_deploy(
        config=config,
        component_names=tag_grouped_repos.keys(),
    )
    try:
        for rule in rules:
            disable_immutable_rule(config=config, project=project, rule=rule)

        for repo_name in tag_grouped_repos:
            deleted_artifacts_count = 0
            tag_groups = tag_grouped_repos[repo_name]
            versions = list(tag_groups.keys())
            stale_versions = versions[config.toolforge_repo_artifact_limit :]
            logger.info(
                f"{config.environment} - Deleting {len(stale_versions)} stale artifacts of {project}/{repo_name}",
            )
            for version in stale_versions:
                tags = tag_groups[version]
                for tag in tags:
                    if tag not in tags_in_use_by_toolforge_deploy.get(repo_name, []):
                        delete_harbor_artifact(config=config, project=project, repo=repo_name, tag=tag)
                        deleted_artifacts_count += 1
                    else:
                        logger.info(
                            f"{config.environment} - Skipping deletion of {project}/{repo_name}:{tag} "
                            "as it is in use by toolforge-deploy",
                        )
            # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
            logger.info(
                f"{config.environment} - Successfully deleted {deleted_artifacts_count} "
                f"stale artifacts of {project}/{repo_name} repository",
            )
    finally:
        for rule in rules:
            enable_immutable_rule(config=config, project=project, rule=rule)


class JobNames(Enum):
    DELETE_EMPTY_TOOL_PROJECTS = "delete-empty-tool-projects"
    MANAGE_IMAGE_RETENTION = "manage-image-retention"
    DELETE_STALE_TOOLFORGE_ARTIFACTS = "delete-stale-toolforge-artifacts"
    MANAGE_HARBOR_PROJECTS_QUOTAS = "manage-harbor-projects-quotas"


def delete_empty_harbor_tool_projects(config: Config):
    """
    Job to delete all tool projects that have zero repository counts.
    """
    logger.info(f"{config.environment} - Cleaning up empty harbor tool projects")
    harbor_projects = get_harbor_projects(config=config)
    tool_projects = [
        harbor_project for harbor_project in harbor_projects if is_tool_project(name=harbor_project["name"])
    ]
    empty_tool_projects = [
        harbor_project["name"] for harbor_project in tool_projects if harbor_project["repo_count"] == 0
    ]
    logger.info(f"{config.environment} - Found {len(empty_tool_projects)} empty harbor tool projects")
    for tool_project in empty_tool_projects:
        try:
            logger.info(f"{config.environment} - Deleting harbor project '{tool_project}'")
            delete_harbor_project(config=config, harbor_project=tool_project)
        except APIError:
            logger.debug(
                f"{config.environment} - Failed to delete harbor project {tool_project}."
                "will skip and retry on next run",
            )
            pass  # Do not interrupt execution when api errors are encountered


def manage_harbor_tool_projects_image_retention_policy(config: Config):
    """
    Job to add or update a tool project's image rention policy if it
    doesn't have one of if the policy template has changed since
    the last time this job was run on the project.
    """
    if getattr(config, "do_retentions", None):
        logger.info(f"{config.environment} - Managing image retention policy for tool projects")

        harbor_projects = get_harbor_projects(config=config)
        tool_projects = [
            harbor_project for harbor_project in harbor_projects if is_tool_project(name=harbor_project["name"])
        ]
        non_empty_tool_projects = [
            harbor_project for harbor_project in tool_projects if harbor_project["repo_count"] > 0
        ]

        for project in non_empty_tool_projects:
            ensure_image_retention_policy_is_set_for_project(config=config, harbor_project=project)
    else:
        logger.info(f"{config.environment} - Skipping retentions check")


def delete_stale_toolforge_artifacts(config: Config):
    """
    Job to delete stale artifacts of toolforge project repositories.
    """
    # the logging text below is being tested for in toolforge-deploy. for details see https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy/-/blob/main/functional-tests/maintain-harbor/delete-stale-toolforge-artifacts.bats
    logger.info(f"{config.environment} - Cleaning up stale artifacts of toolforge project repositories")
    toolforge_project_name = "toolforge"
    toolforge_repos = get_repos(config=config, project=toolforge_project_name)
    repos_with_stale_artifacts = filter_repos_with_stale_artifacts(config=config, repos=toolforge_repos)
    repos_artifacts = get_repos_artifacts(
        config=config, repos=repos_with_stale_artifacts, project=toolforge_project_name
    )
    tag_grouped_repos = group_repos_tags_by_version(
        config=config, artifacts=repos_artifacts, project=toolforge_project_name
    )
    delete_stale_artifacts(config=config, tag_grouped_repos=tag_grouped_repos, project=toolforge_project_name)


def manage_harbor_projects_quotas(config: Config):
    """
    Job to keep projects quotas up-to-date with the quota config.
    """
    note = """
        NOTE: harbor's system-wide default project quota will still be
        applied to newly created projects, atleast until this job runs.
        To change the system-wide default, you need to do so manually as this requires
        system-admin privileges, which the maintain-harbor robot account does not have.
        """

    logger.info(f"{config.environment} - Managing harbor projects quotas")
    logger.info(note)

    default_quota = int(parse_quantity(config.project_quota.default))
    quota_overrides = {
        tool: int(parse_quantity(quota_override)) for tool, quota_override in config.project_quota.overrides.items()
    }
    quotas_to_update = []
    project_names = set()
    logger.debug(f"{config.environment} - Getting all projects quotas")

    try:
        current_quotas = get_harbor_projects_quotas(config)
    except APIError as err:
        logger.error(f"{config.environment} - {err.to_str()}")
        return

    for quota in current_quotas:
        project_name = quota["ref"]["name"]
        project_names.add(project_name)

        if project_name in quota_overrides and quota["hard"]["storage"] != quota_overrides[project_name]:
            quota["hard"]["storage"] = quota_overrides[project_name]
            quotas_to_update.append(quota)

        if project_name not in quota_overrides and quota["hard"]["storage"] != default_quota:
            quota["hard"]["storage"] = default_quota
            quotas_to_update.append(quota)

    for tool in quota_overrides:
        if tool not in project_names:
            logger.warning(
                f"{config.environment} - Project {tool} has quota {quota_overrides[tool]} configured,"
                "but does not exist in harbor. Ensure to remove this project from the quota configuration"
            )

    logger.info(f"{config.environment} - Found {len(quotas_to_update)} quotas to update")
    for quota in quotas_to_update:
        try:
            logger.debug(
                f"{config.environment} - Attempting to update quota with ID {quota['id']} for project {quota['ref']['name']}"
            )
            update_harbor_project_quota(config=config, quota=quota)
            logger.debug(
                f"{config.environment} - Successfully updated quota with ID {quota['id']} for project {quota['ref']['name']}"
            )
        except APIError:
            logger.debug(
                f"{config.environment} - Failed to update quota with ID {quota['id']} for project {quota['ref']['name']}"
            )
