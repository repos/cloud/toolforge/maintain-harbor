from typing import Any, List, Literal, Union

from pydantic import Field, PositiveInt
from pydantic_settings import BaseSettings, SettingsConfigDict


class ProjectQuota(BaseSettings):
    default: Union[str, PositiveInt, Literal[-1]] = Field(
        examples=["2Gi", 1, -1],
    )
    overrides: dict[str, Union[str, PositiveInt, Literal[-1]]] = Field(
        examples=[{"tool-1": "2Gi", "tool-2": 1, "tool-3": -1}],
    )


class Config(BaseSettings):
    model_config = SettingsConfigDict(env_prefix="maintain_harbor_")

    environment: str = Field(examples=["toolsbeta"])
    base_harbor_api: str = Field(examples=["https://harbor.domain.name/api/v2.0"])
    auth_username: str = Field(examples=["my_username"])
    auth_password: str = Field(examples=["my_password"])
    do_retentions: bool = Field(examples=[True])
    toolforge_repo_artifact_limit: PositiveInt = Field(
        examples=[2],
    )  # we are delibrately not using conint(ge=0) here because we expect a non zero positive integer.
    image_retention_policy_template: dict[str, Any] = Field(
        examples=[{"example_image_retention_policy_key": "example_image_retention_policy_value"}],
    )  # we are not strictly typing this field with pydantic because harbor already enforces the schema so no need to duplicate that here.
    project_quota: ProjectQuota = Field(
        examples=[{"default": "2Gi", "overrides": {"tool-1": "2Gi"}}],
    )


def get_example_config():
    """
    Generate human-friendly example config from config json schema
    """
    example_config = Config(
        environment="toolsbeta",
        base_harbor_api="https://harbor.domain.name/api/v2.0",
        auth_username="my_username",
        auth_password="my_password",
        do_retentions=True,
        toolforge_repo_artifact_limit=2,
        image_retention_policy_template={"example_image_retention_policy_key": "example_image_retention_policy_value"},
        project_quota=ProjectQuota(default="2Gi", overrides={"tool-1": "2Gi"}),
    ).model_dump(mode="json")
    return "\n".join([f"MAINTAIN_HARBOR_{key.upper()}={value}" for key, value in example_config.items()])


def stringify_config_validation_errors(errors: List) -> str:
    """
    Convert pydantic validation errors to a string
    """
    error_str = "Config validation failed:\n"
    for error in errors:
        field = ".".join(map(str, error["loc"]))
        error_str += f"\n{field}: {error['msg']}"
    error_str += "\n\nRun with --show-config to view the expected config format."
    error_str += "\nRun with --help for more information."
    return error_str
