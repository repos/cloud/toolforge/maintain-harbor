#!/usr/bin/python3
# SPDX-License-Identifier: Apache-2.0

import logging
import sys
from pathlib import Path

import click
from pydantic import ValidationError

from . import jobs
from .config import Config, get_example_config, stringify_config_validation_errors
from .utils import logger


@click.version_option(prog_name="maintain-harbor", version="0.1.0")
@click.group(
    name="maintain_harbor",
    help="Harbor maintenance jobs for different environments.",
    invoke_without_command=True,
)
@click.option("--show-config", help="Show an example configuration and exit", is_flag=True)
@click.option(
    "-c",
    "--config",
    help=(
        "Path to .env config file. Either provide this or specify the individual "
        "config values as environment variables e.g. MAINTAIN_HARBOR_AUTH_USERNAME"
    ),
    type=Path,
    default=Path(".env"),
)
@click.option("-d", "--debug", help="Turn on debug logging", is_flag=True)
@click.pass_context
def maintain_harbor(ctx: click.Context, show_config: bool, config: Path, debug: bool) -> None:
    ctx.ensure_object(dict)

    # print a user-friendly example config and exit
    if show_config:
        click.echo(get_example_config())
        sys.exit(0)

    try:
        ctx.obj["config"] = Config(_env_file=config, _env_file_encoding="utf-8")  # type: ignore
    except ValidationError as e:
        click.echo(stringify_config_validation_errors(e.errors()), err=True)
        sys.exit(1)

    # show help if no subcommand is given
    if not ctx.invoked_subcommand:
        click.echo(ctx.get_help())
        sys.exit(1)

    # configure logging
    out_handler = logging.StreamHandler(sys.stdout)
    out_handler.setLevel(logging.DEBUG)
    out_handler.addFilter(lambda record: record.levelno in (logging.DEBUG, logging.INFO))

    err_handler = logging.StreamHandler(sys.stderr)
    err_handler.setLevel(logging.ERROR)

    if debug:
        log_lvl = logging.DEBUG
    else:
        log_lvl = logging.INFO

    logging.basicConfig(
        level=log_lvl,
        format="%(asctime)s - %(levelname)s - %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%SZ",
        handlers=[out_handler, err_handler],
    )
    logging.captureWarnings(True)


@maintain_harbor.command(
    name="delete-empty-tool-projects",
    help=f"""

        {jobs.JobNames.DELETE_EMPTY_TOOL_PROJECTS.value}:

        This job deletes all tool projects that have zero repository counts.
        """,
)
@click.pass_context
def delete_empty_tool_projects(ctx: click.Context) -> None:
    logger.info(
        "########## Running job "
        + f"{jobs.JobNames.DELETE_EMPTY_TOOL_PROJECTS.value} on {ctx.obj['config'].environment} harbor"
    )
    jobs.delete_empty_harbor_tool_projects(
        config=ctx.obj["config"],
    )


@maintain_harbor.command(
    name="manage-image-retention",
    help=f"""

        {jobs.JobNames.MANAGE_IMAGE_RETENTION.value}:

        this job adds image retention policy to a tool project if it doesn't have one
        or updates the project's image rention policy if the image retention policy
        template has changed since the last time this job was run on the project.
        """,
)
@click.pass_context
def manage_tool_projects_image_retention_policy(ctx: click.Context) -> None:
    logger.info(
        "########## Running job "
        + f"{jobs.JobNames.MANAGE_IMAGE_RETENTION.value} on {ctx.obj['config'].environment} harbor",
    )
    jobs.manage_harbor_tool_projects_image_retention_policy(
        config=ctx.obj["config"],
    )


@maintain_harbor.command(
    name="delete-stale-toolforge-artifacts",
    help=f"""

        {jobs.JobNames.DELETE_STALE_TOOLFORGE_ARTIFACTS.value}:

        This job deletes all artifacts in the toolforge project that are not within scope of
        toolforge_repo_artifact_limit as defined in the config.
        """,
)
@click.pass_context
def delete_stale_toolforge_artifacts(ctx: click.Context) -> None:
    logger.info(
        "########## Running job "
        + f"{jobs.JobNames.DELETE_STALE_TOOLFORGE_ARTIFACTS.value} "
        + f"on {ctx.obj['config'].environment} harbor"
    )
    jobs.delete_stale_toolforge_artifacts(
        config=ctx.obj["config"],
    )


@maintain_harbor.command(
    name="manage-harbor-projects-quotas",
    help=f"""

        {jobs.JobNames.MANAGE_HARBOR_PROJECTS_QUOTAS.value}:

        This job keeps harbor projects quotas up to date.
        """,
)
@click.pass_context
def manage_harbor_projects_quotas(ctx: click.Context) -> None:
    logger.info(
        "########## Running job "
        + f"{jobs.JobNames.MANAGE_HARBOR_PROJECTS_QUOTAS.value} on {ctx.obj['config'].environment} harbor"
    )
    jobs.manage_harbor_projects_quotas(
        config=ctx.obj["config"],
    )


if __name__ == "__main__":
    maintain_harbor()
