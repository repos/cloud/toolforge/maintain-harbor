# **Tools and Toolsbeta Harbor maintenance jobs lives here**

- Delete Harbor tool projects with no repositories
- Create or update Harbor tool projects image retention policy
- Delete stale toolforge project artifacts (images, helmcharts)
- Manage the default Harbor project quota

## **Development**

After you've cloned this repo and before you make your first changes,
setup pre-commit by running the following on the repo's root folder:

```bash
pip3 install pre-commit
pre-commit install
```

## **Running tests**

Tests are run using [tox](https://tox.readthedocs.io/en/latest/), normally,
and are built on [unittest](https://docs.python.org/3/library/unittest.html). As such, to run
tests, install tox by your favorite method and run the `tox` command at the
top level of this folder.
