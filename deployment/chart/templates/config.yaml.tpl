apiVersion: v1
kind: ConfigMap
metadata:
  name: maintain-harbor-config
  labels:
    {{- include "maintain-harbor.labels" . | nindent 4 }}
data:
  MAINTAIN_HARBOR_ENVIRONMENT: "{{ .Values.environment }}"
  MAINTAIN_HARBOR_BASE_HARBOR_API: "{{ .Values.maintainHarbor.harborAuth.harborURL }}"
  MAINTAIN_HARBOR_DO_RETENTIONS: "{{ .Values.maintainHarbor.doRetentions }}"
  MAINTAIN_HARBOR_TOOLFORGE_REPO_ARTIFACT_LIMIT: "{{ .Values.maintainHarbor.toolforgeRepoArtifactLimit }}"
  MAINTAIN_HARBOR_IMAGE_RETENTION_POLICY_TEMPLATE: |-
    {{ .Values.maintainHarbor.imageRetentionPolicyTemplate | toJson | nindent 4 }}
  MAINTAIN_HARBOR_PROJECT_QUOTA: |-
    {{ .Values.maintainHarbor.projectQuota | toJson | nindent 4 }}
