{{/*
Expand the name of the chart.
*/}}
{{- define "maintain-harbor.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "maintain-harbor.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "maintain-harbor.labels" -}}
helm.sh/chart: {{ include "maintain-harbor.chart" . }}
{{ include "maintain-harbor.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "maintain-harbor.selectorLabels" -}}
app.kubernetes.io/name: {{ include "maintain-harbor.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
