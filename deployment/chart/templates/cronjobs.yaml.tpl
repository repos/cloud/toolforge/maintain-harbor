{{- range .Values.maintainHarbor.cronjobs }}
---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ .name }}
  labels:
    name: {{ .name }}
    {{- include "maintain-harbor.labels" $ | nindent 4 }}
  annotations:
    # use https://github.com/stakater/reloader (via the cert-manager deployment)
    # to restart when certificates get renewed
    configmap.reloader.stakater.com/reload: "maintain-harbor-config"
spec:
  concurrencyPolicy: Forbid
  schedule: "{{ .schedule }}"
  jobTemplate:
    metadata:
      labels:
        name: {{ .name }}
        {{- include "maintain-harbor.selectorLabels" $ | nindent 8 }}
    spec:
      backoffLimit: 0
      template:
        metadata:
          labels:
            name: {{ .name }}
            {{- include "maintain-harbor.selectorLabels" $ | nindent 12 }}
        spec:
          restartPolicy: Never
          topologySpreadConstraints:
            - maxSkew: 1
              topologyKey: kubernetes.io/hostname
              whenUnsatisfiable: ScheduleAnyway
              labelSelector:
                matchLabels:
                  name: {{ .name }}
                  {{- include "maintain-harbor.selectorLabels" $ | nindent 18 }}

          containers:
            - name: job
              image: {{ $.Values.maintainHarbor.image.name }}:{{ $.Values.maintainHarbor.image.tag }}
              imagePullPolicy: {{ $.Values.maintainHarbor.image.pullpolicy }}
              command:
                - python3
                - -m
                - src.maintain_harbor
              args: {{ .args | toJson }}
              envFrom:
                - configMapRef:
                    name: maintain-harbor-config
                - secretRef:
                    name: maintain-harbor-secret

              workingDir: /maintain-harbor
              volumeMounts:
                - name: nsswitch
                  mountPath: /etc/nsswitch.conf
                  readOnly: true
          volumes:
            - name: nsswitch
              hostPath:
                path: /etc/nsswitch.conf
                type: File
{{- end }}
